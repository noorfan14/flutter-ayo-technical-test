import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_match.dart';
import 'package:flutterayotest/models/model_score.dart';

class DetailMatch extends StatefulWidget {
  final Match match;

  DetailMatch({Key key, @required this.match}) : super(key: key);

  @override
  _DetailMatchState createState() => _DetailMatchState(match);
}

class _DetailMatchState extends State<DetailMatch> {
  Match match;
  var dbHelper;
  List<Score> _goalScorers = List();

  _DetailMatchState(match) {
    this.match = match;
  }

  getGoalScorer() async {
    List<Score> list;
    list = await dbHelper.getScores(0, match.id);
    setState(() {
      _goalScorers = list;
      print("calleddd");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();

    getGoalScorer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Match Detail"),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                      onTap: () {},
                      child: Card(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 16.0, horizontal: 8),
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.security,
                                    size: 60,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      "${match.homeTeam}",
                                      style: TextStyle(fontSize: 12),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 16.0, horizontal: 8),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text(
                                        "${match.homeScore}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24),
                                      ),
                                      Text(
                                        " - ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24),
                                      ),
                                      Text(
                                        "${match.awayScore}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 24),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "${match.date}",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 16.0, horizontal: 8),
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.security,
                                    size: 60,
                                  ),
                                  Container(
                                    width: 80,
                                    child: Text(
                                      "${match.awayTeam}",
                                      style: TextStyle(fontSize: 12),
                                      textAlign: TextAlign.center,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, top: 16),
              child: Text(
                "GoalScorer",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataTable(
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    'Name',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Minutes',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Team',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ],
              rows: _goalScorers
                  .map((e) => DataRow(cells: [
                        DataCell(
                          Text("${e.playerName}"),
                        ),
                        DataCell(
                          Text(
                            "${e.minuteAt}'",
                            style: TextStyle(color: Colors.green),
                          ),
                        ),
                        DataCell(
                          Text("${e.teamName}"),
                        ),
                      ]))
                  .toList(),
            ),
          ],
        ),
      ),
    );
  }
}
