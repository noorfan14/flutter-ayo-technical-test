import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_match.dart';
import 'package:flutterayotest/pages/matches/add_match.dart';
import 'package:flutterayotest/pages/matches/detail_match.dart';
import 'package:flutterayotest/pages/matches/report_match.dart';

class Matches extends StatefulWidget {
  @override
  _MatchesState createState() => _MatchesState();
}

class _MatchesState extends State<Matches> {
  List<Match> _matchesUpcoming = List();
  List<Match> _matchesFinished = List();
  var dbHelper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
    getMatchesUpcoming();
    getMatchesFinished();
  }

  getMatchesUpcoming() async {
    List<Match> list;
    list = await dbHelper.getMatchesUpcoming(0, 0);
    setState(() {
      _matchesUpcoming = list;
    });
  }

  getMatchesFinished() async {
    List<Match> list;
    list = await dbHelper.getMatchesFinished(0, 1);
    setState(() {
      _matchesFinished = list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            String result = await Navigator.push(
                context, MaterialPageRoute(builder: (context) => AddMatch()));
            if (result == "fromAdd") {
              getMatchesUpcoming();
              getMatchesFinished();
            }
          },
          child: Icon(Icons.add),
        ),
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                text: "Upcoming",
              ),
              Tab(
                text: "Finished",
              ),
            ],
          ),
          title: Text('Matches'),
        ),
        body: TabBarView(
          children: [
            (_matchesUpcoming.length == 0)
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Data Empty,",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        Text(
                          "Add a match and the data will shown here.",
                        ),
                      ],
                    ),
                  )
                : ListView.builder(
                    itemCount: _matchesUpcoming.length,
                    itemBuilder: (context, index) {
                      return upcomingMatches(_matchesUpcoming[index]);
                    }),
            (_matchesFinished.length == 0)
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Data Empty,",
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        Text(
                          "Finish a match and the data will shown here.",
                        ),
                      ],
                    ),
                  )
                : ListView.builder(
                    itemCount: _matchesFinished.length,
                    itemBuilder: (context, index) {
                      return finishedMatches(_matchesFinished[index]);
                    }),
          ],
        ),
      ),
    );
  }

  Widget upcomingMatches(Match match) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                confirmationDialog(context, match);
              },
              child: Card(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.security,
                            size: 60,
                          ),
                          Container(
                            width: 80,
                            child: Text(
                              "${match.homeTeam}",
                              style: TextStyle(fontSize: 12),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Text(
                            "${match.time} WIB",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Text(
                            "${match.date}",
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.security,
                            size: 60,
                          ),
                          Container(
                              width: 80,
                              child: Text(
                                "${match.awayTeam}",
                                style: TextStyle(fontSize: 12),
                                textAlign: TextAlign.center,
                              ))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget finishedMatches(Match match) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => DetailMatch(match: match)));
              },
              child: Card(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.security,
                            size: 60,
                          ),
                          Container(
                            width: 80,
                            child: Text(
                              "${match.homeTeam}",
                              style: TextStyle(fontSize: 12),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                "${match.homeScore}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                              Text(
                                " - ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                              Text(
                                "${match.awayScore}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                            ],
                          ),
                          Text(
                            "${match.date}",
                            style: TextStyle(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical:16.0, horizontal: 8),
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.security,
                            size: 60,
                          ),
                          Container(
                            width: 80,
                            child: Text(
                              "${match.awayTeam}",
                              style: TextStyle(fontSize: 12),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void confirmationDialog(BuildContext context, Match match) {
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () async {
        Navigator.pop(context);

        String result = await
        Navigator.push(context, MaterialPageRoute(builder: (context) => ReportMatch(match: match,)));
        if(result == "fromReport") {
          getMatchesUpcoming();
          getMatchesFinished();
        }
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Do you want to finish this match?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
