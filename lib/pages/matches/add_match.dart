import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_match.dart';
import 'package:flutterayotest/models/model_team.dart';
import 'package:flutterayotest/pages/manage_data/players/form_player.dart';

class AddMatch extends StatefulWidget {
  @override
  _FormTeamState createState() => _FormTeamState();
}

class _FormTeamState extends State<AddMatch> {
  String _date, _hour, _minutes;
  TextEditingController _dateController = TextEditingController();

  final formKey = new GlobalKey<FormState>();
  var dbHelper;

  TeamDropdown homeTeam, awayTeam;
  String currentPosition;
  List<TeamDropdown> _teams = List();

  getTeams() async {
    List<Team> list;
    list = await dbHelper.getTeams(0);
    setState(() {
      for (var team in list) {
        _teams.add(TeamDropdown(team.id, team.name));
      }
    });
  }

  DateTime selectedDate = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _dateController.text = "${selectedDate.toLocal()}".split(' ')[0];
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
    getTeams();
  }

  void validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
      FocusScope.of(context).requestFocus(FocusNode());
      formKey.currentState.save();
      Match e = Match(null, _date, "$_hour:$_minutes", homeTeam.name,
          awayTeam.name, 0, 0, 0, 0);
      dbHelper.saveMatch(e);
      Navigator.pop(context, "fromAdd");
    } else {
      print('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Matches"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(
                        "Add Match.",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      )),
                  TextFormField(
                    controller: _dateController,
                    onTap: () {
                      _selectDate(context);
                    },
                    enabled: true,
                    validator: (value) =>
                        value.isEmpty ? 'Date cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _date = value,
                    decoration: InputDecoration(
                        labelText: "Date",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Date"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: Text("Time"),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        width: 100,
                        child: TextFormField(
                          validator: (value) =>
                              value.isEmpty ? 'Hour cannot be blank' : null,
                          onSaved: (value) => _hour = value,
                          maxLength: 2,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              counter: Container(),
                              labelText: "Hour",
//                        prefixIcon: Icon(Icons.lock),
                              hintText: "Hour"),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: 100,
                        child: TextFormField(
                          validator: (value) =>
                              value.isEmpty ? 'Minutes cannot be blank' : null,
                          onSaved: (value) => _minutes = value,
                          maxLength: 2,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                              counter: Container(),
                              labelText: "Minutes",
//                        prefixIcon: Icon(Icons.lock),
                              hintText: "Minutes"),
                        ),
                      ),
                      Text(
                        "WIB",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text("Home Team")),
                  Container(
                    height: 50,
                    child: DropdownButton(
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        isExpanded: true,
                        hint: Text("Select here"),
                        value: homeTeam,
                        items: _teams.map((TeamDropdown team) {
                          return new DropdownMenuItem<TeamDropdown>(
                            value: team,
                            child: new Text(
                              team.name,
                              style: new TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                        onChanged: (TeamDropdown val) {
                          print("selected team : ${val.id} ${val.name}");
                          setState(() {
                            homeTeam = val;
                          });
                        }),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text("Away Team")),
                  Container(
                    height: 50,
                    child: DropdownButton(
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        isExpanded: true,
                        hint: Text("Select here"),
                        value: awayTeam,
                        items: _teams.map((TeamDropdown team) {
                          return new DropdownMenuItem<TeamDropdown>(
                            value: team,
                            child: new Text(
                              team.name,
                              style: new TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                        onChanged: (TeamDropdown val) {
                          print("selected team : ${val.id} ${val.name}");
                          setState(() {
                            awayTeam = val;
                          });
                        }),
                  ),
//                  TextFormField(
//                    validator: (value) =>
//                    value.isEmpty ? 'Address cannot be blank' : null,
//                    onSaved: (value) => _address = value,
//                    decoration: InputDecoration(
//                        labelText: "Address",
////                        prefixIcon: Icon(Icons.account_circle),
//                        hintText: "Address"),
//                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Submit",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Theme.of(context).primaryColor),
                        ),
                        RaisedButton(
                          shape: CircleBorder(),
                          color: Theme.of(context).primaryColor,
                          onPressed: validateAndSave,
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
