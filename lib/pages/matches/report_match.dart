import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_match.dart';
import 'package:flutterayotest/models/model_score.dart';
import 'package:flutterayotest/models/model_team.dart';
import 'package:flutterayotest/pages/manage_data/players/form_player.dart';

class TeamSideDropdown {
  const TeamSideDropdown(this.id, this.name, this.isAway);

  final int id;
  final int isAway;
  final String name;
}

class ReportMatch extends StatefulWidget {
  final Match match;

  ReportMatch({Key key, @required this.match}) : super(key: key);

  @override
  _ReportMatchState createState() => _ReportMatchState(match);
}

class _ReportMatchState extends State<ReportMatch> {
  String _date, _hour, _minutes, _homeScore, _awayScore, _playerName, _minuteAt;
  TextEditingController _playerController,
      _minuteAtController = TextEditingController();

  final formKey = new GlobalKey<FormState>();
  Team team;
  Match match;
  var dbHelper;

  _ReportMatchState(match) {
    this.match = match;
  }

  String currentPosition;
  TeamSideDropdown homeTeam, awayTeam, selectedTeam;
  List<TeamSideDropdown> _teams = List();
  List<Score> _goalScorers = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();

    _teams.add(TeamSideDropdown(1, match.homeTeam, 0));
    _teams.add(TeamSideDropdown(2, match.awayTeam, 1));
    getGoalScorer();
  }

  getGoalScorer() async {
    List<Score> list;
    list = await dbHelper.getScores(0, match.id);
    setState(() {
      _goalScorers = list;
      print("calleddd");
    });
  }

  void validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
      FocusScope.of(context).requestFocus(FocusNode());
      formKey.currentState.save();
      Match e = Match(match.id, match.date, "$_hour:$_minutes", match.homeTeam,
          match.awayTeam, int.parse(_homeScore), int.parse(_awayScore), 1, 0);
      dbHelper.updateMatch(e);
      Navigator.pop(context, "fromReport");
    } else {
      print('Form is invalid');
    }
  }

  void addGoalScorer() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
      FocusScope.of(context).requestFocus(FocusNode());
      formKey.currentState.save();
      Score e = Score(null, match.id, "1", _playerName, selectedTeam.name,
          int.parse(_minuteAt), selectedTeam.isAway, 0);
      dbHelper.saveScore(e);
      getGoalScorer();
      _playerController.text = "";
      _minuteAtController.text = "";
    } else {
      print('Form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Matches"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(
                        "Match Report.",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      )),
                  TextFormField(
                    enabled: false,
                    initialValue: "${match.date}",
                    validator: (value) =>
                        value.isEmpty ? 'Date Team cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _date = value,
                    decoration: InputDecoration(
                        labelText: "Date",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Date"),
                  ),
                  TextFormField(
                    enabled: false,
                    initialValue: "${match.time} WIB",
                    validator: (value) =>
                        value.isEmpty ? 'Time cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _date = value,
                    decoration: InputDecoration(
                        labelText: "Time",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Time"),
                  ),
                  TextFormField(
                    enabled: false,
                    initialValue: "${match.homeTeam}",
                    validator: (value) =>
                        value.isEmpty ? 'Home Team cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _date = value,
                    decoration: InputDecoration(
                        labelText: "Home Team",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Home Team"),
                  ),
                  TextFormField(
                    enabled: false,
                    initialValue: "${match.awayTeam}",
                    validator: (value) =>
                        value.isEmpty ? 'Away Team cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _date = value,
                    decoration: InputDecoration(
                        labelText: "Away Team",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Away Team"),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                    validator: (value) =>
                        value.isEmpty ? 'Home Score cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _homeScore = value,
                    decoration: InputDecoration(
                        counter: Container(),
                        labelText: "Home Score",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Home Score"),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                    validator: (value) =>
                        value.isEmpty ? 'Away Score cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _awayScore = value,
                    decoration: InputDecoration(
                        counter: Container(),
                        labelText: "Away Score",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Away Score"),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text(
                        "Goal Scorer",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text("Select Team")),
                  Container(
                    height: 50,
                    child: DropdownButton(
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        isExpanded: true,
                        hint: Text("Select here"),
                        value: selectedTeam,
                        items: _teams.map((TeamSideDropdown team) {
                          return new DropdownMenuItem<TeamSideDropdown>(
                            value: team,
                            child: new Text(
                              team.name,
                              style: new TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                        onChanged: (TeamSideDropdown val) {
                          print("selected team : ${val.id} ${val.name}");
                          setState(() {
                            selectedTeam = val;
                          });
                        }),
                  ),
                  TextFormField(
//                    validator: (value) =>
//                        value.isEmpty ? 'Player Name cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    controller: _playerController,
                    onSaved: (value) => _playerName = value,
                    decoration: InputDecoration(
                        counter: Container(),
                        labelText: "Player Name",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Player Name"),
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 3,
                    controller: _minuteAtController,
//                    validator: (value) =>
//                        value.isEmpty ? 'At Minutes cannot be blank' : null,
//                    initialValue: "${selectedDate.toLocal()}".split(' ')[0],
                    onSaved: (value) => _minuteAt = value,
                    decoration: InputDecoration(
                        counter: Container(),
                        labelText: "Minutes",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Minutes"),
                  ),
                  Container(
                    width: double.infinity,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Theme.of(context).primaryColor,
                      onPressed: addGoalScorer,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          "Add",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  DataTable(
                    columns: const <DataColumn>[
                      DataColumn(
                        label: Text(
                          'Name',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Minutes',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Team',
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      ),
                    ],
                    rows: _goalScorers
                        .map((e) => DataRow(cells: [
                              DataCell(
                                Text("${e.playerName}"),
                              ),
                              DataCell(
                                Text(
                                  "${e.minuteAt}'",
                                  style: TextStyle(color: Colors.green),
                                ),
                              ),
                              DataCell(
                                Text("${e.teamName}"),
                              ),
                            ]))
                        .toList(),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0, bottom: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Save Report",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Theme.of(context).primaryColor),
                        ),
                        RaisedButton(
                          shape: CircleBorder(),
                          color: Theme.of(context).primaryColor,
                          onPressed: validateAndSave,
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
