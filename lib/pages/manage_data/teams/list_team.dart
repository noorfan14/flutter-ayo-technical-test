import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/Utility.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_team.dart';
import 'package:flutterayotest/pages/manage_data/teams/form_team.dart';
import 'package:flutterayotest/pages/manage_data/teams/detail_team.dart';

class ListTeamPage extends StatefulWidget {
  @override
  _ListTeamState createState() => _ListTeamState();
}

class _ListTeamState extends State<ListTeamPage> {
  var dbHelper;
  List<Team> _teams = List();

  Widget appBarTitle = new Text(
    "Teams",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchQuery = new TextEditingController();
  bool _IsSearching;
  String _searchText = "";

  void _handleSearchStart() {
    setState(() {
      _IsSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Teams",
        style: new TextStyle(color: Colors.white),
      );
      _IsSearching = false;
      _searchQuery.clear();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
    getTeams();
  }

  getTeams() async {
    List<Team> list;
    list = await dbHelper.getTeams(0);
    setState(() {
      _teams = list;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: appBarTitle,
//        actions: <Widget>[
//          new IconButton(
//            icon: actionIcon,
//            onPressed: () {
//              setState(() {
//                if (this.actionIcon.icon == Icons.search) {
//                  this.actionIcon = new Icon(
//                    Icons.close,
//                    color: Colors.white,
//                  );
//                  this.appBarTitle = new TextField(
//                    controller: _searchQuery,
//                    style: new TextStyle(
//                      color: Colors.white,
//                    ),
//                    decoration: new InputDecoration(
//                        prefixIcon: new Icon(Icons.search, color: Colors.white),
//                        hintText: "Search...",
//                        hintStyle: new TextStyle(color: Colors.white)),
//                  );
//                  _handleSearchStart();
//                } else {
//                  _handleSearchEnd();
//                }
//              });
//            },
//          ),
//        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          String result = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FormTeam(
                        isUpdating: false,
                      )));
          setState(() {
            if (result == "fromAdd") {
              getTeams();
            }
          });
        },
        child: Icon(Icons.add),
      ),
      body: Container(
//          padding: EdgeInsets.symmetric(horizontal: 16),
          child: (_teams.length == 0)
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Data Empty,",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                      Text(
                        "Add a team and the data will shown here.",
                      ),
                    ],
                  ),
                )
              : ListView.separated(
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: _teams.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () async {
                        String result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailTeam(
                                      team: _teams[index],
                                    )));
                        setState(() {
                          if (result == "fromDetail") {
                            getTeams();
                          }
                        });
                      },
                      title: Text(
                        _teams[index].name,
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black.withOpacity(0.7)),
                      ),
                      subtitle: Text(_teams[index].cityBase),
                      leading: (_teams[index].logo != "")
                          ? Container(
                              width: 50,
                              height: 50,
                              child: Utility.imageFromBase64String(
                                  _teams[index].logo))
                          : Icon(
                              Icons.security,
                              size: 50,
                            ),
                    );
                  })),
    );
  }
}
