import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/Utility.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_team.dart';
import 'package:image_picker/image_picker.dart';

class FormTeam extends StatefulWidget {
  final bool isUpdating;
  final Team team;

  FormTeam({Key key, @required this.isUpdating, this.team}) : super(key: key);

  @override
  _FormTeamState createState() => _FormTeamState(isUpdating, team);
}

class _FormTeamState extends State<FormTeam> {
  String _name, _since, _address, _city;

  final formKey = new GlobalKey<FormState>();
  var dbHelper;
  bool isUpdating;
  Team team;
  String selectedImage = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();

    if(isUpdating) {
      selectedImage = team.logo;
    }
  }

  _FormTeamState(isUpdating, team) {
    this.isUpdating = isUpdating;
    this.team = team;
  }

  void validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
      FocusScope.of(context).requestFocus(FocusNode());
      formKey.currentState.save();
      if (isUpdating) {
        Team e = Team(team.id, _name, selectedImage, _since, _address, _city, 0);
        dbHelper.updateTeam(e);
        Navigator.pop(context, e);
      } else {
        Team e = Team(null, _name, selectedImage, _since, _address, _city, 0);
        dbHelper.saveTeam(e);
        Navigator.pop(context, "fromAdd");
      }
    } else {
      print('Form is invalid');
    }
  }

  pickImageFromGallery() {
    ImagePicker.pickImage(source: ImageSource.gallery).then((imgFile) {
      String imgString = Utility.base64String(imgFile.readAsBytesSync());
      setState(() {
        selectedImage = imgString;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Teams"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 20, bottom: 10),
                      child: Text(
                        (isUpdating) ? "Edit Team." : "Add Team.",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      )),
                  InkWell(
                    onTap: pickImageFromGallery,
                    child: Center(
                      child: Stack(
                        alignment: Alignment.center,
                        children: <Widget>[
                          Container(
                            height: 100,
                            width: 100,
                            child: Utility.imageFromBase64String(selectedImage),
                          ),
                          Center(
                            child: Text((selectedImage != "") ? "" : "tap here to select logo"),
                          ),
                        ],
                      ),
                    ),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? team.name : "",
                    validator: (value) =>
                        value.isEmpty ? 'Name cannot be blank' : null,
                    onSaved: (value) => _name = value,
                    decoration: InputDecoration(
                        labelText: "Name",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Name"),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? team.since : "",
                    validator: (value) =>
                        value.isEmpty ? 'Since cannot be blank' : null,
                    onSaved: (value) => _since = value,
                    keyboardType: TextInputType.number,
                    maxLength: 4,
                    decoration: InputDecoration(
                        labelText: "Since",
                        counter: Container(),
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Since"),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? team.cityBase : "",
                    validator: (value) =>
                        value.isEmpty ? 'City cannot be blank' : null,
                    onSaved: (value) => _city = value,
                    decoration: InputDecoration(
                        labelText: "City Base",
//                        prefixIcon: Icon(Icons.lock),
                        hintText: "City Base"),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? team.address : "",
                    validator: (value) =>
                        value.isEmpty ? 'Address cannot be blank' : null,
                    onSaved: (value) => _address = value,
                    decoration: InputDecoration(
                        labelText: "Address",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Address"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Submit",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Theme.of(context).primaryColor),
                        ),
                        RaisedButton(
                          shape: CircleBorder(),
                          color: Theme.of(context).primaryColor,
                          onPressed: validateAndSave,
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
