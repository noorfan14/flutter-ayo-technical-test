import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_player.dart';
import 'package:flutterayotest/pages/manage_data/players/form_player.dart';
import 'package:flutterayotest/pages/manage_data/players/detail_player.dart';
import 'package:flutterayotest/pages/manage_data/teams/detail_team.dart';

class ListPlayerByTeamPage extends StatefulWidget {
  final int teamId;
  ListPlayerByTeamPage({Key key, @required this.teamId}) : super(key: key);

  @override
  _ListPlayerByTeamState createState() => _ListPlayerByTeamState(teamId);
}

class _ListPlayerByTeamState extends State<ListPlayerByTeamPage> {
  List<Player> _players = List();
  var dbHelper;
  int teamId;

  _ListPlayerByTeamState(teamId) {
    this.teamId = teamId;
  }

  Widget appBarTitle = new Text(
    "Players",
    style: new TextStyle(color: Colors.white),
  );
  Icon actionIcon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final key = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchQuery = new TextEditingController();
  List<String> _list;
  bool _IsSearching;
  String _searchText = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
    getPlayers();
  }

  getPlayers() async {
    List<Player> list;
    list = await dbHelper.getPlayersByTeam(0, teamId);
    setState(() {
      _players = list;
    });
  }

  void _handleSearchStart() {
    setState(() {
      _IsSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.actionIcon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Players",
        style: new TextStyle(color: Colors.white),
      );
      _IsSearching = false;
      _searchQuery.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: appBarTitle,
//        actions: <Widget>[
//          new IconButton(
//            icon: actionIcon,
//            onPressed: () {
//              setState(() {
//                if (this.actionIcon.icon == Icons.search) {
//                  this.actionIcon = new Icon(
//                    Icons.close,
//                    color: Colors.white,
//                  );
//                  this.appBarTitle = new TextField(
//                    controller: _searchQuery,
//                    style: new TextStyle(
//                      color: Colors.white,
//                    ),
//                    decoration: new InputDecoration(
//                        prefixIcon: new Icon(Icons.search, color: Colors.white),
//                        hintText: "Search...",
//                        hintStyle: new TextStyle(color: Colors.white)),
//                  );
//                  _handleSearchStart();
//                } else {
//                  _handleSearchEnd();
//                }
//              });
//            },
//          ),
//        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          String result = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => FormPlayer(isUpdating: false)));
          setState(() {
            if (result == "fromAdd") {
              getPlayers();
            }
          });
        },
        child: Icon(Icons.add),
      ),
      body: Container(
//          padding: EdgeInsets.symmetric(horizontal: 16),
          child: (_players.length == 0)
              ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Data Empty,",
                  style: TextStyle(fontWeight: FontWeight.w600),
                ),
                Text(
                  "Add a player and the data will shown here.",
                ),
              ],
            ),
          )
              : ListView.separated(
              separatorBuilder: (context, index) => Divider(),
              itemCount: _players.length,
              itemBuilder: (context, index) {
                return ListTile(
                  onTap: () async {
                    String result =  await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailPlayer(player: _players[index],)));
                    setState(() {
                      if (result == "fromDetail") {
                        getPlayers();
                      }
                    });
                  },
                  title: Text(
                    _players[index].name,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.black.withOpacity(0.7)),
                  ),
                  subtitle: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(_players[index].position),
                      Text((_players[index].teamName == null) ? "${_players[index].teamName}" : "${_players[index].teamName}"),
                    ],
                  ),
                  leading: Container(
                    color: Theme.of(context).primaryColor,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        _players[index].playerNumber,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                );
              })),
    );
  }
}
