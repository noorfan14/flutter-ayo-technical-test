import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/Utility.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_team.dart';
import 'package:flutterayotest/pages/manage_data/players/list_player.dart';
import 'package:flutterayotest/pages/manage_data/teams/form_team.dart';
import 'package:flutterayotest/pages/manage_data/teams/list_player_by_team.dart';

class DetailTeam extends StatefulWidget {
  final Team team;

  DetailTeam({Key key, @required this.team}) : super(key: key);
  @override
  _DetailTeamState createState() => _DetailTeamState(team);
}

class _DetailTeamState extends State<DetailTeam> {
  Team team;
  var dbHelper;

  _DetailTeamState(team) {
    this.team = team;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        print('back pressed');
        Navigator.pop(context, "fromDetail");
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: ()async{
            Navigator.pop(context,"fromDetail");
          }),
          centerTitle: true,
          title: Text("Teams"),
          actions: <Widget>[IconButton(icon: Icon(Icons.edit), onPressed: () async{
            Team result = await Navigator.push(
                context, MaterialPageRoute(builder: (context) => FormTeam(isUpdating: true, team: team,)));
            setState(() {
              if (result != null) {
                team = result;
              }
            });
          })],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            confirmationDialog(context);
          },
          child: Icon(Icons.delete),
        ),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            width: double.infinity,
//          padding: EdgeInsets.symmetric(horizontal: 16),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 150,
                        height: 150,
                        margin: EdgeInsets.only(top: 100, bottom: 10),
                        child: (team.logo != "") ? Utility.imageFromBase64String(team.logo) : Icon(
                          Icons.security,
                          size: 150,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Team Name",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(team.name),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Since",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(team.since),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "City Base",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(team.cityBase),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Address",
                        style: TextStyle(fontWeight: FontWeight.w600),
                      ),
                    ),
                    Text(team.address),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ListPlayerByTeamPage(teamId: team.id,)));
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Icon(
                                  Icons.people,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  "See Players",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600),
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }

  void confirmationDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed:  () {
        Team e = Team(team.id, team.name, "", team.since, team.address, team.cityBase, 1);
        dbHelper.updateTeam(e);
        Navigator.pop(context);
        Navigator.pop(context, "fromDetail");
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Do you want to delete this team?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }


}
