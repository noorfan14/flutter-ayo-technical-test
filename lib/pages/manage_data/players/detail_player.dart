import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_player.dart';
import 'package:flutterayotest/pages/manage_data/players/form_player.dart';

class DetailPlayer extends StatefulWidget {
  final Player player;

  DetailPlayer({Key key, @required this.player}) : super(key: key);

  @override
  _DetailPlayerState createState() => _DetailPlayerState(player);
}

class _DetailPlayerState extends State<DetailPlayer> {
  Player player;
  var dbHelper;

  _DetailPlayerState(player) {
    this.player = player;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        print('back pressed');
        Navigator.pop(context, "fromDetail");
        return true;
      },
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () async {
                Navigator.pop(context, "fromDetail");
              }),
          centerTitle: true,
          title: Text("Players"),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.edit),
                onPressed: () async {
                  Player result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FormPlayer(
                                isUpdating: true,
                                player: player,
                              )));
                  setState(() {
                    if (result != null) {
                      player = result;
                    }
                  });
                })
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            confirmationDialog(context);
          },
          child: Icon(Icons.delete),
        ),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            width: double.infinity,
//          padding: EdgeInsets.symmetric(horizontal: 16),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                      child: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 100),
                            child: Text(
                              "Player Number",
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.black.withOpacity(0.7),
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Container(
                            color: Theme.of(context).primaryColor,
                            margin: EdgeInsets.only(top: 10),
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 24.0, vertical: 16.0),
                                  child: Text(
                                    player.playerNumber,
                                    style: TextStyle(
                                        fontSize: 50,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text(
                            "Name",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text(player.name),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            "Height",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text("${player.height} cm"),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            "Weight",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text("${player.weight} kg"),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            "Position",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text(player.position),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Text(
                            "Player",
                            style: TextStyle(fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text(player.teamName),
                      ],
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }

  void confirmationDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Yes"),
      onPressed: () {
        Player e = Player(
            player.id,
            player.name,
            player.height,
            player.weight,
            player.position,
            player.playerNumber,
            player.teamId,
            player.name,
            1);
        dbHelper.updatePlayer(e);
        Navigator.pop(context);
        Navigator.pop(context, "fromDetail");
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Do you want to delete this player?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
