import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutterayotest/helper/db_helper.dart';
import 'package:flutterayotest/models/model_player.dart';
import 'package:flutterayotest/models/model_team.dart';

class TeamDropdown {
  const TeamDropdown(this.id, this.name);

  final int id;
  final String name;
}

class FormPlayer extends StatefulWidget {
  final bool isUpdating;
  final Player player;

  FormPlayer({Key key, @required this.isUpdating, this.player})
      : super(key: key);

  @override
  _FormPlayerState createState() => _FormPlayerState(isUpdating, player);
}

class _FormPlayerState extends State<FormPlayer> {
  String _name, _height, _weight, _position, _playerNumber;

  final formKey = new GlobalKey<FormState>();
  var dbHelper;
  bool isUpdating;
  Player player;
  TeamDropdown currentTeam;
  String currentPosition;
  List<TeamDropdown> _teams = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbHelper = DBHelper();
    getTeams();
  }

  getTeams() async {
    List<Team> list;
    list = await dbHelper.getTeams(0);
    setState(() {
      for (var team in list) {
        _teams.add(TeamDropdown(team.id, team.name));
      }
      if (isUpdating) {
        currentTeam =
            _teams.firstWhere((element) => element.id == player.teamId);
        currentPosition = player.position;
      }
    });
  }

  _FormPlayerState(isUpdating, player) {
    this.isUpdating = isUpdating;
    this.player = player;
  }

  void validateAndSave() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      print('Form is valid');
      FocusScope.of(context).requestFocus(FocusNode());
      formKey.currentState.save();
      savePlayer(_playerNumber);
    } else {
      print('Form is invalid');
    }
  }

  savePlayer(String playerNumber) async {
    Player e = Player(null, _name, _height, _weight, currentPosition,
        _playerNumber, currentTeam.id, currentTeam.name, 0);
    if (isUpdating) {
      e.id = player.id;
      dbHelper.updatePlayer(e);
      Navigator.pop(context, e);
    } else {
      List<Player> list;
      list = await dbHelper.checkPlayersNumber(0, currentTeam.id, playerNumber);
      setState(() {
        if (list.length > 0) {
          Flushbar(
            backgroundColor: Colors.red,
            title: "Number already used",
            message: "Please try another number",
            duration: Duration(seconds: 3),
          )..show(context);
        } else {
          print("not found");
          dbHelper.savePlayer(e);
          Navigator.pop(context, "fromAdd");
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Players"),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: Text(
                        (isUpdating) ? "Edit Player." : "Add Player.",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      )),
                  TextFormField(
                    initialValue: (isUpdating) ? player.name : "",
                    validator: (value) =>
                        value.isEmpty ? 'Name cannot be blank' : null,
                    onSaved: (value) => _name = value,
                    decoration: InputDecoration(
                        labelText: "Name",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Name"),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? player.height : "",
                    validator: (value) =>
                        value.isEmpty ? 'Height cannot be blank' : null,
                    onSaved: (value) => _height = value,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Height (cm)",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Height (cm)"),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? player.weight : "",
                    validator: (value) =>
                        value.isEmpty ? 'Weight cannot be blank' : null,
                    onSaved: (value) => _weight = value,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Weight (kg)",
//                        prefixIcon: Icon(Icons.lock),
                        hintText: "Weight (kg)"),
                  ),
//                  TextFormField(
//                    initialValue: (isUpdating) ? player.position : "",
//                    validator: (value) =>
//                        value.isEmpty ? 'Position cannot be blank' : null,
//                    onSaved: (value) => _position = value,
//                    decoration: InputDecoration(
//                        labelText: "Position",
////                        prefixIcon: Icon(Icons.account_circle),
//                        hintText: "Position"),
//                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text("Select Position")),
                  DropdownButton<String>(
                    onTap: () {
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    hint: Text("Select here"),
                    value: currentPosition,
                    isExpanded: true,
                    onChanged: (String newValue) {
                      FocusScope.of(context).unfocus();
                      setState(() {
                        currentPosition = newValue;
                      });
                    },
                    items: <String>[
                      'Goalkeeper',
                      'Right Back',
                      'Left Back',
                      'Center Back',
                      'Midfielder',
                      'Winger',
                      'Striker'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                  TextFormField(
                    initialValue: (isUpdating) ? player.playerNumber : "",
                    validator: (value) =>
                        value.isEmpty ? 'Player Number cannot be blank' : null,
                    onSaved: (value) => _playerNumber = value,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Player Number",
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Player Number"),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 16),
                      child: Text("Select Team")),
                  Container(
                    height: 50,
                    child: DropdownButton(
                        onTap: () {
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        isExpanded: true,
                        hint: Text("Select here"),
                        value: currentTeam,
                        items: _teams.map((TeamDropdown team) {
                          return new DropdownMenuItem<TeamDropdown>(
                            value: team,
                            child: new Text(
                              team.name,
                              style: new TextStyle(color: Colors.black),
                            ),
                          );
                        }).toList(),
                        onChanged: (TeamDropdown val) {
                          FocusScope.of(context).unfocus();
                          print("selected team : ${val.id} ${val.name}");
                          setState(() {
                            currentTeam = val;
                          });
                        }),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Submit",
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Theme.of(context).primaryColor),
                        ),
                        RaisedButton(
                          shape: CircleBorder(),
                          color: Theme.of(context).primaryColor,
                          onPressed: validateAndSave,
                          child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Icon(
                              Icons.send,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
