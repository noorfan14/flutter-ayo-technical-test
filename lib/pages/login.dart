import 'package:flutter/material.dart';
import 'package:flutterayotest/pages/home/dashboard.dart';
import 'package:flutterayotest/pages/home/home.dart';
import 'package:flutterayotest/pages/register.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 100),
                    child: Text(
                      "Welcome to \nXYZ APPS !",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 40,
                          color: Theme.of(context).primaryColor),
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.email),
                        hintText: "Email"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock), hintText: "Password"),
                    obscureText: true,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Sign in",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      ),
                      RaisedButton(
                        shape: CircleBorder(),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Dashboard()));
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Icon(
                            Icons.send,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Didn't have an Account?"),
                      InkWell(onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Register()));
                      }, child: Text(" Register", style: TextStyle(color: Theme.of(context).primaryColor),))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
