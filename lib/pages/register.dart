import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 100),
                    child: Text(
                      "Register Account.",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 30,
                          color: Theme.of(context).primaryColor),
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Username"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "First Name"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
//                        prefixIcon: Icon(Icons.account_circle),
                        hintText: "Last Name"),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 16),
                  child: TextField(
                    decoration: InputDecoration(
//                        prefixIcon: Icon(Icons.lock),
                        hintText: "Password"),
                    obscureText: true,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Submit",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 30,
                            color: Theme.of(context).primaryColor),
                      ),
                      RaisedButton(
                        shape: CircleBorder(),
                        color: Theme.of(context).primaryColor,
                        onPressed: () {},
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Icon(
                            Icons.send,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Already have an Account?"),
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            " Login",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor),
                          ))
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
