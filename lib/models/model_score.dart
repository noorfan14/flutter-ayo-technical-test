class Score {
  int id;
  int matchId;
  String playerId;
  String playerName;
  String teamName;
  int minuteAt;
  int isAway;
  int isDeleted;

  Score(this.id, this.matchId, this.playerId, this.playerName,this.teamName, this.minuteAt, this.isAway, this.isDeleted);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'match_id': matchId,
      'player_id': playerId,
      'player_name': playerName,
      'team_name': teamName,
      'minuteAt': minuteAt,
      'is_away': isAway,
      'is_deleted': isDeleted
    };
    return map;
  }

  Score.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    matchId = map['match_id'];
    playerId = map['player_id'];
    playerName = map['player_name'];
    teamName = map['team_name'];
    minuteAt = map['minuteAt'];
    isAway = map['is_away'];
    isDeleted = map['is_deleted'];
  }

}