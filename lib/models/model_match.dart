class Match {
  int id;
  String date;
  String time;
  String homeTeam;
  String awayTeam;
  int homeScore;
  int awayScore;
  int status;
  int isDeleted;

  Match(this.id, this.date, this.time, this.homeTeam, this.awayTeam, this.homeScore, this.awayScore, this.status, this.isDeleted);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'date': date,
      'time': time,
      'home_team': homeTeam,
      'away_team': awayTeam,
      'home_score': homeScore,
      'away_score': awayScore,
      'status': status,
      'is_deleted': isDeleted
    };
    return map;
  }

  Match.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    date = map['date'];
    time = map['time'];
    homeTeam = map['home_team'];
    awayTeam = map['away_team'];
    homeScore = map['home_score'];
    awayScore = map['away_score'];
    status = map['status'];
    isDeleted = map['is_deleted'];
  }

}