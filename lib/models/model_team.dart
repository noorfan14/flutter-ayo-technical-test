class Team {
  int id;
  String name;
  String logo;
  String since;
  String address;
  String cityBase;
  int isDeleted;

  Team(this.id, this.name, this.logo, this.since, this.address, this.cityBase, this.isDeleted);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'name': name,
      'logo': logo,
      'since': since,
      'address': address,
      'city_base': cityBase,
      'is_deleted': isDeleted
    };
    return map;
  }

  Team.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    logo = map['logo'];
    since = map['since'];
    address = map['address'];
    cityBase = map['city_base'];
    isDeleted = map['is_deleted'];
  }

}
