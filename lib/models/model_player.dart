class Player {
  int id;
  String name;
  String height;
  String weight;
  String position;
  String playerNumber;
  int teamId;
  String teamName;
  int isDeleted;

  Player(this.id, this.name, this.height, this.weight, this.position,
      this.playerNumber, this.teamId, this.teamName, this.isDeleted);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'name': name,
      'height': height,
      'weight': weight,
      'position': position,
      'player_number': playerNumber,
      'team_id': teamId,
      'team_name': teamName,
      'is_deleted': isDeleted
    };
    return map;
  }

  Player.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    height = map['height'];
    weight = map['weight'];
    position = map['position'];
    playerNumber = map['player_number'];
    teamId = map['team_id'];
    teamName = map['team_name'];
    isDeleted = map['is_deleted'];
  }
}
