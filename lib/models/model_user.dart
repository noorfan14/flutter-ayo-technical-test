class User {
  int id;
  String username;
  String firstName;
  String lastName;
  String password;
  bool isDeleted;

  User(this.id, this.username, this.firstName, this.lastName, this.password, this.isDeleted);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'username': username,
      'first_name': firstName,
      'last_name': lastName,
      'password': password,
      'is_deleted': isDeleted
    };
    return map;
  }

  User.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    username = map['username'];
    firstName = map['first_name'];
    lastName = map['last_name'];
    password = map['password'];
    isDeleted = map['is_deleted'];
  }

}
