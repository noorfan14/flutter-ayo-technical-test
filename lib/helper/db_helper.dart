import 'dart:async';
import 'dart:io' as io;
import 'package:flutterayotest/models/model_match.dart';
import 'package:flutterayotest/models/model_player.dart';
import 'package:flutterayotest/models/model_score.dart';
import 'package:flutterayotest/models/model_user.dart';
import 'package:flutterayotest/pages/matches/matches.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutterayotest/models/model_team.dart';

class DBHelper {
  static Database _db;
  static const String TABLE_USER = 'users';
  static const String TABLE_TEAM = 'teams';
  static const String TABLE_PLAYER = 'players';
  static const String TABLE_MATCH = 'matches';
  static const String TABLE_SCORE = 'scores';
  static const String DB_NAME = 'xyz.db';

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDB();
    return _db;
  }

  initDB() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $TABLE_USER (id INTEGER PRIMARY KEY, username TEXT, first_name TEXT, last_name TEXT, password TEXT, is_deleted BOOL)");
    await db.execute(
        "CREATE TABLE $TABLE_TEAM (id INTEGER PRIMARY KEY, name TEXT, logo TEXT, since TEXT, address TEXT, city_base TEXT, is_deleted BOOL)");
    await db.execute(
        "CREATE TABLE $TABLE_PLAYER (id INTEGER PRIMARY KEY, name TEXT, height TEXT, weight TEXT, position TEXT, player_number TEXT, team_id INTEGER, team_name TEXT, is_deleted BOOL)");
    await db.execute(
        "CREATE TABLE $TABLE_MATCH (id INTEGER PRIMARY KEY, date TEXT, time TEXT, home_team TEXT, away_team TEXT, home_score INTEGER, away_score INTEGER, status INTEGER, is_deleted BOOL)");
    await db.execute(
        "CREATE TABLE $TABLE_SCORE (id INTEGER PRIMARY KEY, match_id INTEGER, player_id TEXT, player_name TEXT, team_name TEXT, minuteAt INTEGER, is_away BOOL, is_deleted BOOL)");
  }

  //user
  Future<User> saveUser(User user) async {
    var dbClient = await db;
    user.id = await dbClient.insert(TABLE_USER, user.toMap());
    return user;
  }

  Future<List<User>> getUser() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_USER, columns: [
      'id',
      'username',
      'first_name',
      'last_name',
      'password',
      'is_deleted'
    ]);
    List<User> users = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        users.add(User.fromMap(maps[i]));
      }
    }
    return users;
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    return await dbClient.update(TABLE_USER, user.toMap(),
        where: 'id = ?', whereArgs: [user.id]);
  }

  Future<Player> savePlayer(Player player) async {
    var dbClient = await db;
    player.id = await dbClient.insert(TABLE_PLAYER, player.toMap());
    return player;
  }

  Future<List<Player>> getPlayers(int isDeleted) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_PLAYER,
        columns: [
          'id',
          'name',
          'height',
          'weight',
          'position',
          'player_number',
          'team_id',
          'team_name',
          'is_deleted'
        ],
        where: 'is_deleted = ?',
        whereArgs: [isDeleted]);
    List<Player> players = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        players.add(Player.fromMap(maps[i]));
      }
    }
    return players;
  }

  Future<List<Player>> getPlayersByTeam(int isDeleted, int teamId) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_PLAYER,
        columns: [
          'id',
          'name',
          'height',
          'weight',
          'position',
          'player_number',
          'team_id',
          'team_name',
          'is_deleted'
        ],
        where: 'is_deleted = ? AND team_id = ?',
        whereArgs: [isDeleted, teamId]);
    List<Player> players = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        players.add(Player.fromMap(maps[i]));
      }
    }
    return players;
  }

  Future<List<Player>> checkPlayersNumber(int isDeleted, int teamId, String playerNumber ) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_PLAYER,
        columns: [
          'id',
          'name',
          'height',
          'weight',
          'position',
          'player_number',
          'team_id',
          'team_name',
          'is_deleted'
        ],
        where: 'is_deleted = ? AND team_id = ? AND player_number = ?',
        whereArgs: [isDeleted, teamId, playerNumber]);
    List<Player> players = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        players.add(Player.fromMap(maps[i]));
      }
    }
    return players;
  }

  Future<int> updatePlayer(Player player) async {
    var dbClient = await db;
    return await dbClient.update(TABLE_PLAYER, player.toMap(),
        where: 'id = ?', whereArgs: [player.id]);
  }

  Future<Team> saveTeam(Team team) async {
    var dbClient = await db;
    team.id = await dbClient.insert(TABLE_TEAM, team.toMap());
    return team;
  }

  Future<List<Team>> getTeams(int isDeleted) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_TEAM,
        columns: [
          'id',
          'name',
          'logo',
          'since',
          'address',
          'city_base',
          'is_deleted'
        ],
        where: 'is_deleted = ?',
        whereArgs: [isDeleted]);
    List<Team> teams = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        teams.add(Team.fromMap(maps[i]));
      }
    }
    return teams;
  }

  Future<int> updateTeam(Team team) async {
    var dbClient = await db;
    return await dbClient.update(TABLE_TEAM, team.toMap(),
        where: 'id = ?', whereArgs: [team.id]);
  }

  Future<Match> saveMatch(Match match) async {
    var dbClient = await db;
    match.id = await dbClient.insert(TABLE_MATCH, match.toMap());
    print("saved here ${match.homeTeam} ${match.status}");
    return match;
  }

  Future<List<Match>> getMatchesUpcoming(int isDeleted, int status) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_MATCH,
        columns: [
          'id',
          'date',
          'time',
          'home_team',
          'away_team',
          'home_score',
          'away_score',
          'status',
          'is_deleted'
        ],
        where: 'is_deleted = ? AND status = ?',
        whereArgs: [isDeleted, status]);
    List<Match> matches = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print("upcoming");
        matches.add(Match.fromMap(maps[i]));
      }
    }
    return matches;
  }

  Future<List<Match>> getMatchesFinished(int isDeleted, int status) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_MATCH,
        columns: [
          'id',
          'date',
          'time',
          'home_team',
          'away_team',
          'home_score',
          'away_score',
          'status',
          'is_deleted'
        ],
        where: 'is_deleted = ? AND status = ?',
        whereArgs: [isDeleted, status]);
    List<Match> matches = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        print("finished");
        matches.add(Match.fromMap(maps[i]));
      }
    }
    return matches;
  }

  Future<int> updateMatch(Match match) async {
    var dbClient = await db;
    print("update match");
    return await dbClient.update(TABLE_MATCH, match.toMap(),
        where: 'id = ?', whereArgs: [match.id]);
  }

  Future<Score> saveScore(Score score) async {
    var dbClient = await db;
    score.id = await dbClient.insert(TABLE_SCORE, score.toMap());
    print("saved score ${score.playerName}");
    return score;
  }

  Future<List<Score>> getScores(int isDeleted, int matchId) async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLE_SCORE,
        columns: [
          'id',
          'match_id',
          'player_id',
          'player_name',
          'team_name',
          'minuteAt',
          'is_away',
          'is_deleted'
        ],
        where: 'is_deleted = ? AND match_id = ?',
        whereArgs: [isDeleted, matchId]);
    List<Score> scores = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        scores.add(Score.fromMap(maps[i]));
      }
    }
    return scores;
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
